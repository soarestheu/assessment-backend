<?php

class Logs
{

    private $conexao;

    ######### CONSTRUTOR:
    public function __construct()
    {
        $this->conexao = new MySQLi (_SERV,_USER,_PSW,_BD) or die("Erro ao conectar com o banco de dados.");
        $this->conexao->set_charset("utf8");
    }

    // Cadastro de Logs
    public function CadLog($acao, $descricao)
    {
        $sql = $this->conexao->prepare("INSERT INTO logs(acao,descricao) VALUES (?,?)");
        $sql->bind_param('ss', $acao, $descricao);
        $sql->execute();
        $sql->close();
    }

    // Pega todos os Logs cadastrados
    public function GetLog(){
        $sql = $this->conexao->prepare("SELECT acao, descricao, dataAlteracao FROM logs");
        $sql->execute();
        $sql->bind_result($acao_, $desc_, $data_);
        while ($sql->fetch()) {
            $dados['acao'][]      =   $acao_;
            $dados['desc'][]      =   $desc_;
            $dados['data'][]      =   $data_;
        }
        $sql->close();
        return($dados);
    }
}
