<?php 
require_once("Log.php");
class Produto extends Logs
{

    private $conexao;

    ######### CONSTRUTOR:
    public function __construct()
    {
        $this->conexao = new MySQLi (_SERV,_USER,_PSW,_BD) or die("Erro ao conectar com o banco de dados.");
        $this->conexao->set_charset("utf8");
        parent::__construct();
    }

    // CADASTRO DO PRODUTO:
    public function CadProduto($nome, $cod, $preco, $desc, $qnt, $imagem=NULL)
    {
        $sql = $this->conexao->prepare("INSERT INTO produto (nome,cod,preco,descricao,quantidade,imagem) 
                                        VALUES (?,?,?,?,?,?)");
        $sql->bind_param('ssdsds', $nome, $cod, $preco, $desc, $qnt, $imagem);
        if ($sql->execute()) {
            $a="Cadastro do produto -> $nome";
            $b="Usuário realizou cadastro de um novo produto";
            $this->CadLog($a,$b);
            $idProd = $sql->insert_id;
            return($idProd);
        } else {
            return(0);
        }
        $sql->close();
    }

    // Pega todo os produtos cadastrados
    public function GetProdutos()
    {
        $sql = $this->conexao->prepare("SELECT id, nome, cod, preco, descricao, quantidade, imagem FROM produto");
        $sql->execute();
        $sql->bind_result($id_, $nome_, $cod_, $preco_, $desc_, $qnt_, $imagem_);
        while ($sql->fetch()) {
            $dados['id'][]      =   $id_;
            $dados['nome'][]    =   $nome_;
            $dados['cod'][]     =   $cod_;
            $dados['preco'][]   =   $preco_;
            $dados['desc'][]    =   $desc_;
            $dados['qnt'][]     =   $qnt_;
            if ($imagem_) {
                $dados['imagem'][]  =   $imagem_;
            } else {
                $dados['imagem'][]  =   "telaVazia.jpg";
            }
        }
        $sql->close();
        return($dados);
    }

    // Pega o produto atraves do id
    public function GetProdutoID($idProd)
    {
        $sql = $this->conexao->prepare("SELECT id, nome, cod, preco, descricao, quantidade,imagem FROM produto WHERE id=?");
        $sql->bind_param('d', $idProd);
        $sql->execute();
        $sql->bind_result($id_, $nome_, $cod_, $preco_, $desc_, $qnt_, $imagem_);
        while ($sql->fetch()) {
            $dados['id']      =   $id_;
            $dados['nome']    =   $nome_;
            $dados['cod']     =   $cod_;
            $dados['preco']   =   $preco_;
            $dados['desc']    =   $desc_;
            $dados['qnt']     =   $qnt_;
            if ($imagem_) {
                $dados['imagem']  =   $imagem_;
            } else {
                $dados['imagem']  =   "telaVazia.jpg";
            }
        }
        $sql->close();
        return($dados);
    }

    // Edita o produto escolhido
    public function EditaProduto($nome, $cod, $preco, $desc, $qnt, $id)
    {
        $sql = $this->conexao->prepare("UPDATE produto SET nome = ?, cod = ?, preco = ?, descricao = ?, quantidade = ? WHERE id = ?");
        $sql->bind_param('ssdsdd',$nome, $cod, $preco, $desc, $qnt, $id);
        if ($sql->execute()) {
            $this->CadLog("Atualização do produto -> $nome","Usuário editou o produto");
            return(1);
        } else {
            return(0);
        }
        $sql->close();
    }

    // Apaga o produto escolhido
    public function ApagaProduto($idProd)
    {
        $sql = $this->conexao->prepare("DELETE FROM produto WHERE id = ?");
        $sql->bind_param('d',$idProd);
        if ($sql->execute()) {
            $nomeProd = $this->GetProdutoID($idProd);
            $this->CadLog("Exclusão do produto -> ".$nomeProd['nome'],"Usuário realizou exclusão de um produto");
            return(1);
        } else {
            return(0);
        }
        $sql->close();
    }
}

