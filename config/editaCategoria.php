<?php

// Arquivo de conexao
require_once("../includes/conexao.php");
// Arquivo da classe categoria
require_once("../classes/Categoria.php");

// Instanciando objetoss
$Categoria    = new Categoria();


$idCat         = $_POST['id'];
$nomeCat       = $_POST['category-name'];
$codCat        = $_POST['category-code'];

$edita = $Categoria->EditaCategoria($idCat, $nomeCat, $codCat);

if ($edita) {
    $msg = "Categoria editada com sucesso!";
    $link = "window.location.href='../?page=cadastraCategoria&id=$idCat'";
} else {
    $msg = "Erro ao editar a categoria!";
    $link = "window.history.back()";
}

echo "<script>alert('$msg');$link;</script>";

