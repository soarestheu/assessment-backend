<?php

// Arquivo de conexao
require_once("../includes/conexao.php");
// Arquivo das classes
require_once("../classes/Produto.php");
require_once("../classes/Catalogar.php");

// Instanciando objetoss
$Produto    = new Produto();
$Catalogar  = new Catalogar();

$idProd         = $_POST['id'];
$nomeProd       = $_POST['nome'];
$skuProd        = $_POST['sku'];
$preco          = $_POST['preco'];
$quantidade     = $_POST['qnt'];
$categoria      = $_POST['categoria'];
$descricao      = $_POST['descricao'];

$edita = $Produto->EditaProduto($nomeProd, $skuProd, $preco, $descricao, $quantidade, $idProd);

if($edita && ($Catalogar->EditaCatProd($idProd, $categoria))){
    $msg = "Produto editado com sucesso!";
    $link = "window.location.href='../?page=cadastraProduto&id=$idProd'";
} else {
    $msg = "Erro ao editar o Produto!";
    $link = "window.history.back()";
}

echo "<script>alert('$msg');$link;</script>";

