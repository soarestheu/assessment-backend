<?php
// error_reporting(0);
// Arquivo de conexao
require_once("includes/conexao.php");
// Arquivos das classes
require_once("classes/Catalogar.php");
require_once("classes/Categoria.php");
require_once("classes/Produto.php");
require_once("classes/Log.php");

// Instanciando objetos
$Log = new Logs();
$Categoria  = new Categoria();
$Produto    = new Produto();
$Catalogar  = new Catalogar();

require_once("includes/header.php");

switch($_GET['page']){
    case 'cadastraCategoria':   require_once("pages/addCategory.php");  break;
    case 'cadastraProduto':     require_once("pages/addProduct.php");   break;
    case 'produto':             require_once("pages/products.php");     break;
    case 'categoria':           require_once("pages/categories.php");   break;
    case 'dashboard':           require_once("pages/dashboard.php");    break;
    case 'inicio':              require_once("pages/dashboard.php");    break;
    case 'importaArquivo':      require_once("pages/importa.php");      break;
    case 'logs':                require_once("pages/logs.php");         break;
    
    default: require_once("pages/dashboard.php");    break;
}

require_once("includes/footer.php");


?>