<?php

if (isset($_GET['id'])) {
  $idCat    = $_GET['id'];
  $cat      = $Categoria->GetCategoriaID($idCat);
  $link     = "editaCategoria.php";
} else {
  $link     = "cadCategoria.php";
}
?>
<!-- Header -->
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Nova Categoria</h1>
    
    <form action="config/<?=$link;?>" method="post">
      <div class="input-field">
        <label for="category-name" class="label">Nome da categoria</label>
        <input type="text" id="category-name" name="category-name" class="input-text" value="<?=$cat['nome']?>" />
        
      </div>
      <div class="input-field">
        <label for="category-code" class="label">Código da categoria</label>
        <input type="text" id="category-code" name="category-code" class="input-text"  value="<?=$cat['cod']?>" />
        
      </div>
      <div class="actions-form">
        <input type="hidden" name="id" value="<?=$idCat;?>" >
        <a href="?page=categoria" class="action back">Voltar</a>
        <input class="btn-submit btn-action"  type="submit" value="Cadastrar" />
      </div>
    </form>
  </main>
  <!-- Main Content -->