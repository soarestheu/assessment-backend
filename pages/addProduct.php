<?php

if (isset($_GET['id'])) {
  $idProd   = $_GET['id'];
  $editProd = $Produto->GetProdutoID($idProd);
  $catEdit  = $Catalogar->GetCatProdID($idProd);
  $link     = "editaProduto.php";
}else{
  $link     = "cadProduto.php";
}

$cat = $Categoria->GetCategoria();
?>
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Novo Produto</h1>
    
    <form action="config/<?=$link;?>" method="post" enctype="multipart/form-data">
      <div class="input-field">
        <label for="sku" class="label" alt="Código">SKU do produto</label>
        <input type="text" id="sku" name="sku" required class="input-text" value="<?=$editProd['cod'];?>" /> 
      </div>
      <div class="input-field">
        <label for="name" class="label">Nome do produto:</label>
        <input type="text" id="name" name='nome' required class="input-text" value="<?=$editProd['nome'];?>"/> 
      </div>
      <?php if (!$idProd) { ?>
        <div class="input-field">
          <label for="name" class="label">Imagem:</label>
          <input type="file" id="imagemProduto" name='imagemProduto' class="input-text" /> 
        </div>
      <?php } ?>
      <div class="input-field">
        <label for="price" class="label">Preço</label>
        <input type="text" id="price" name="preco" required class="input-text" value="<?=$editProd['preco'];?>" /> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantidade:</label>
        <input type="text" id="quantity" name="qnt" required class="input-text" value="<?=$editProd['qnt'];?>" /> 
      </div>
      <div class="input-field">
        <label for="category" class="label">Categorias</label>
        <select multiple id="category" name="categoria[]" class="input-text">
        <?php for( $i = 0; $i < count($cat['id']); $i++ ) { ?>
            <option value="<?=$cat['id'][$i];?>" ><?=$cat['nome'][$i];?></option>
        <?php } ?>
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label">Descrição</label>
        <textarea id="description" name="descricao" class="input-text" ><?=$editProd['desc'];?></textarea>
      </div>
      <div class="actions-form">
        <input type="hidden" name="id" value="<?=$idProd;?>">
        <a href="?page=produto" class="action back">Voltar</a>
        <input class="btn-submit btn-action" type="submit" value="Cadastrar Produto" />
      </div>
      
    </form>
  </main>
  <!-- Main Content -->

 