<!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Nova Categoria</h1>
    
    <form action="importaCSV/importaArq.php" method="post" enctype="multipart/form-data">
      <div class="input-field">
        <label for="category-name" class="label">Arquivo para importação de dados(formato CSV): </label>
        <input type="file" id="arquivo" name='arquivo' class="input-text" /> 
      </div>
      <div class="actions-form">
        <a href="?page=inicio" class="action back">Voltar</a>
        <input class="btn-submit btn-action"  type="submit" value="Importar" />
      </div>
    </form>
  </main>
  <!-- Main Content -->