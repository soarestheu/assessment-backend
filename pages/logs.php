<?php

$logs = $Log->GetLog();
?>
<!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Logs de ação</h1>
    </div>
        <table  class="data-grid"  id="table_id" class="display">
            <thead>
          <tr class="data-row">
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Ações do usuário</span>
            </th>
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Descrição</span>
            </th>
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Data da ação</span>
            </th>
          </tr>
        </thead>
        <tbody>
        <?php for ( $i = 0; $i < count($logs['acao']); $i++) { ?>
          <tr class="data-row">
            <td class="data-grid-td">
              <span class="data-grid-cell-content"><?=$logs['acao'][$i];?></span>
            </td>
            <td class="data-grid-td">
              <span class="data-grid-cell-content"><?=$logs['desc'][$i]; ?></span>
            </td>
            <td class="data-grid-td">
              <span class="data-grid-cell-content"><?=$logs['data'][$i]; ?></span>
            </td>
          </tr>
        <?php } ?>
        </tbody>
    </table>
  </main>
  <!-- Main Content -->