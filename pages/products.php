<?php
$prod = $Produto->GetProdutos();
for ( $i = 0; $i < count($prod['id']); $i++) {
  $prodCategoria[$prod['id'][$i]]  = $Catalogar->GetCategoriaProd($prod['id'][$i]);
}
?>
<script>
function ExcluiProduto(idProd){
  var resp = confirm("Tem certeza que deseja apagar esse produto ?");
  if (resp) {
    location.href = "config/apagaProduto.php?id="+idProd;
  }
}
</script>
<!-- Main Content -->
<main class="content">
  <div class="header-list-page">
    <h1 class="title">Produto</h1>
    <a href="?page=cadastraProduto" class="btn-action">Adicionar novo produto</a>
  </div>
  <table class="data-grid">
    <thead>
      <tr class="data-row">
        <th class="data-grid-th">
          <span class="data-grid-cell-content">Nome</span>
        </th>
        <th class="data-grid-th">
          <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
          <span class="data-grid-cell-content">Preço</span>
        </th>
        <th class="data-grid-th">
          <span class="data-grid-cell-content">Quantidade</span>
        </th>
        <th class="data-grid-th">
          <span class="data-grid-cell-content">Categoria</span>
        </th>

        <th class="data-grid-th">
          <span class="data-grid-cell-content">Ações</span>
        </th>
      </tr>
    </thead>
    <tbody>
      <?php for ( $k=0; $k < count($prod['id']); $k++) { ?>
      <tr class="data-row">
        <td class="data-grid-td">
          <span class="data-grid-cell-content"><?=$prod['nome'][$k];?></span>
        </td>
      
        <td class="data-grid-td">
          <span class="data-grid-cell-content"><?=$prod['cod'][$k];?></span>
        </td>

        <td class="data-grid-td">
          <span class="data-grid-cell-content">R$ <?=$prod['preco'][$k];?></span>
        </td>

        <td class="data-grid-td">
          <span class="data-grid-cell-content"><?=$prod['qnt'][$k];?></span>
        </td>

        <td class="data-grid-td">
          <span class="data-grid-cell-content">
            <?php for ($l=0; $l < count($prodCategoria[$prod['id'][$k]]); $l++) { ?>
              <?=$prodCategoria[$prod['id'][$k]][$l];?>
              <br/>
            <?php } ?>
          </span>
        </td>
        <td class="data-grid-td">
          <div class="actions">
            <div class="action edit">
              <span>
                <a href="?page=cadastraProduto&id=<?=$prod['id'][$k];?>">
                  Editar
                </a>
              </span>
            </div>
            <div class="action delete">
              <span>
                <a href="javascript:ExcluiProduto(<?=$prod['id'][$k];?>)">
                  Deletar
                </a>
              </span>
            </div>
          </div>
        </td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</main>
<!-- Main Content -->


